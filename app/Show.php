<?php

/**
 * Created by Reliese Model.
 */

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Http;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

/**
 * Class Show
 * 
 * @property int $id
 * @property string $name
 * @property float $popularity
 * @property int $vote_count
 * @property bool $video
 * @property string $poster_path
 * @property bool $adult
 * @property string $backdrop_path
 * @property string $original_language
 * @property string $origin_country
 * @property string $original_name
 * @property float $vote_average
 * @property string $overview
 * @property Carbon $first_air_date
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class Show extends Model
{
	protected $table = 'shows';
	public $incrementing = false;

	protected $casts = [
		'id' => 'int',
		'popularity' => 'float',
		'vote_count' => 'int',
		'video' => 'bool',
		'adult' => 'bool',
		'vote_average' => 'float'
	];

	protected $dates = [
		'first_air_date'
	];

	protected $fillable = [
		'id',
		'name',
		'popularity',
		'vote_count',
		'video',
		'poster_path',
		'adult',
		'backdrop_path',
		'original_language',
		'origin_country',
		'original_name',
		'vote_average',
		'overview',
		'first_air_date',
		'season_number'
	];

	public $genres;
	public $credits;
	public $videos;
	public $images;
	public $seasons;

	public static function getPopular()
    {
        $popularShowsArray = Http::withToken(
            config('services.tmdb.token')
        )
        ->get('https://api.themoviedb.org/3/tv/popular')
		->json()['results'];
		
		$popularShows = collect($popularShowsArray)->map(function($data){
			$show = new Show();
			$genresShow = Arr::pull($data,'genre_ids');
			$genres = Genre::getShowsGenres();


			$show->genres = collect($genres)->filter(function($genre) use ($genresShow) {
				if(in_array($genre->id, $genresShow))
				{
					return ($genre);
				}		
			});
			return $show->fill(Arr::except($data,['genre_ids']));
		});
		
		return $popularShows;
	}

    public static function getNowPlaying()
    {
        $nowPlayingArray = Http::withToken(
            config('services.tmdb.token')
        )
        ->get('https://api.themoviedb.org/3/tv/now_playing')
		->json()['results'];
		
		$nowPlaying = collect($nowPlayingArray)->map(function($data) {
			$show = new Show();
			$genresShow = Arr::pull($data,'genre_ids');
			$genres = Genre::getShowsGenres();

			$show->genres = collect($genres)->filter(function($genre) use ($genresShow) {
				if(in_array($genre->id, $genresShow))
				return ($genre);
			});

			return $show->fill($data);
		});
		return $nowPlaying;
	}

	public function episodes(int $season = null)
    {
		if($season === null)
		{
			$seasonEpisodes = collect();
			foreach($this->seasons as $season)
			{
				$episodes = [];
				for($i =1; $i<$season['episode_count'];$i++) {
					$episodes[] = Http::withToken(
						config('services.tmdb.token')
					)
					->get('https://api.themoviedb.org/3/tv/'.$this->id.'/season/'.$season['season_number'].'/episode/'.$i)
					->json();
				}
				$seasonEpisodes[$season['season_number']]= ($episodes);
			}
			return $seasonEpisodes;
		} else {
			for($i =0; $i<$this->seasons[$season]['episode_count'];$i++) {
				$episodes [] = Http::withToken(
					config('services.tmdb.token')
				)
				->get('https://api.themoviedb.org/3/tv/'.$this->id.'/season/'.$season.'/episode/'.$i)
				->json();
			}
			return $episodes;
		}
	}

	public function episode(int $season, int $episode)
    {
			return Http::withToken(
				config('services.tmdb.token')
			)
			->get('https://api.themoviedb.org/3/tv/'.$this->id.'/season/'.$season.'/episode/'.$episode)
			->json();
	}

	public static function show($id)
    {
		$movieArray = Http::withToken(
            config('services.tmdb.token')
        )
        ->get('https://api.themoviedb.org/3/tv/'.$id.'?append_to_response=credits,videos,images')
		->json();
		die();
			$show = new Show(); 
			$show->credits =  collect(Arr::pull($movieArray,'credits'));
			$show->videos =  collect(Arr::pull($movieArray,'videos'));
			$show->images =  collect(Arr::pull($movieArray,'images'));
			$show->seasons =  collect(Arr::pull($movieArray,'seasons'));
			$genresMovie = Arr::pull($movieArray,'genres');
			$genres = Genre::getMoviesGenres();
			$show->genres = collect($genres)->filter(function($genre) use ($genresMovie) {
				if(in_array($genre->id, $genresMovie))
				{
					return ($genre);
				}
			});		
			return $show->fill(Arr::except($movieArray,['genres']));
	}

	public function image($width = 500)
    {  
        return "https://image.tmdb.org/t/p/w$width/$this->poster_path";
    }

	public function genre()
	{
		return $this->belongsTo('App\Genre','id_genre');
	}

	/* recuperer les commentaires les plus recents*/
	public function comments()
	{
		// return $this->morphMany('App\Comment', 'commentable')->latest();
		return $this->hasMany('App\Comment', 'commentable_id');
		
	}
}
