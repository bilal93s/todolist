<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Exception;
use Carbon\Carbon;
use App\Http\Services\EmailService;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TodoList
 *
 * @property int $id
 * @property string $name
 * @property int $id_user
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class TodoList extends Model
{

	protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
	];

	protected $fillable = [
		'name',
		'description'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function items()
    {
        return $this->hasMany(Item::class)->orderBy('created_at', 'desc');
    }

    public function isValid(): bool
    {
        return !empty($this->name)
            && strlen($this->name) <= 255
            && (is_null($this->description) || strlen($this->description) <= 255);
    }

    /**
     * @param Item $item
     * @return Item
     * @throws Exception
     */
    public function canAddItem(Item $item): Item
    {
        if (is_null($item) || !$item->isValid()) {
            throw new Exception('Item est vide ou invalide');
        }

        if (is_null($this->user) || !$this->user->isValid()) {
            throw new Exception('User est vide ou invalide');
        }

        if ($this->itemsCount() >= 10) {
            throw new Exception('Votre TodoList contient trop ditem');
        }

        $lastItem = $this->getLastItem();
        if (!is_null($this->getLastItem()) && Carbon::now()->subMinutes(30)->isBefore($lastItem->created_at)) {
            throw new Exception('Il faut patientez 30min entre lajout de chaque item');
        }

        return $item;
    }

    protected function getLastItem(): ?Item
    {
        return $this->items->first();
    }

    protected function itemsCount()
    {
        return sizeof($this->items()->get());
    }
}
