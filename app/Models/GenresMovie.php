<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class GenresMovie
 * 
 * @property int $id
 * @property int $id_movie
 * @property int $id_genre
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class GenresMovie extends Model
{
	protected $table = 'genres_movies';

	protected $casts = [
		'id_movie' => 'int',
		'id_genre' => 'int'
	];

	protected $fillable = [
		'id_movie',
		'id_genre'
	];
}
