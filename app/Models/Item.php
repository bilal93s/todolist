<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Exception;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Item
 *
 * @property int $id
 * @property int $id_todo_list
 * @property string $name
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class Item extends Model
{
	protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
	];

	protected $fillable = [
        'name',
        'content',
        'created_at'
    ];

    public function todoList()
    {
        return $this->belongsTo(TodoList::class);
    }

    public function isValid()
    {
        return !empty($this->name)
        && !empty($this->content)
        && strlen($this->content) <= 1000;
    }
}
