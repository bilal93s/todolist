<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Show
 * 
 * @property int $id
 * @property string $name
 * @property float $popularity
 * @property int $vote_count
 * @property bool $video
 * @property string $poster_path
 * @property bool $adult
 * @property string $backdrop_path
 * @property string $original_language
 * @property string $origin_country
 * @property string $original_name
 * @property float $vote_average
 * @property string $overview
 * @property Carbon $first_air_date
 * @property int $season_number
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class Show extends Model
{
	protected $table = 'shows';
	public $incrementing = false;

	protected $casts = [
		'id' => 'int',
		'popularity' => 'float',
		'vote_count' => 'int',
		'video' => 'bool',
		'adult' => 'bool',
		'vote_average' => 'float',
		'season_number' => 'int'
	];

	protected $dates = [
		'first_air_date'
	];

	protected $fillable = [
		'name',
		'popularity',
		'vote_count',
		'video',
		'poster_path',
		'adult',
		'backdrop_path',
		'original_language',
		'origin_country',
		'original_name',
		'vote_average',
		'overview',
		'first_air_date',
		'season_number'
	];
}
