<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Post;
use App\Film;
use App\Seance;
use App\Personne;
use App\Fonction;
use App\Salle;

use Illuminate\Database\Eloquent\Builder;

class SeancesController extends Controller
{
    public function seances()
    {
        $seances = Seance::all();

        $films = Film::all();

        $salles = Salle::all();
        
        $ouvreurs = Personne::with('fonctions')->whereHas('fonctions', function (Builder $query) {
            $query->where('nom', '=', 'hotesse');
        })->get();

        $techniciens = Personne::with('fonctions')->whereHas('fonctions', function (Builder $query) {
            $query->where('nom', '=', 'projectionniste');
        })->get();

        $nettoyeurs = Personne::with('fonctions')->whereHas('fonctions', function (Builder $query) {
            $query->where('nom', '=', 'agent entretien');
        })->get();
        

        return view('seances',['films' => $films,'seances' => $seances, 'salles' => $salles, 'films' => $films, 'ouvreurs' => $ouvreurs, 'techniciens' => $techniciens, 'nettoyeurs' => $nettoyeurs]);
    }

    public function add_seance(Request $request)
    {
    
        $seance = new Seance;
        $film = Film::find($request->film);

        $seance->id_film = $request->film;
        $seance->id_salle = $request->salle;
        $seance->id_personne_ouvreur = $request->ouvreur;
        $seance->id_personne_technicien = $request->technicien;
        $seance->id_personne_menage = $request->nettoyeur;
        $seance->debut_seance = Carbon::create($request->debut_seance);
        $seance->fin_seance = $request->debut_seance->addMinutes($film->duree_minutes);

        $seance->save();
        return redirect('seances');
    }
}
