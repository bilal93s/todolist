<?php

namespace App\Http\Controllers;

use Faker\ORM\CakePHP\Populator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class MoviesController extends Controller
{
    //
    public function index()
    {   
        $popularMovies = $this->getPopularMovies();
        $popularSeries = $this->getPopularSeries();

        return view('movies', ['popularMovies'=>$popularMovies,'popularSeries'=>$popularSeries]);
    }

    public function getPopularMovies()
    {
        return Http::withToken(
            config('services.tmdb.token')
        )
        ->get('https://api.themoviedb.org/3/movie/popular')
        ->json()['results'];
    }
    public function getPopularSeries()
    {
        return Http::withToken(
            config('services.tmdb.token')
        )
        ->get('https://api.themoviedb.org/3/tv/popular')
        ->json()['results'];
    }

    public function getImage($poster_path, $width = 500)
    {  
        return "https://image.tmdb.org/t/p/w$width/$poster_path";
    }

    public function getGenreName()
    {
        $genreArray = Http::withToken(
            config('services.tmdb.token')
        )
        ->get('https://api.themoviedb.org/3/genre/movie/list')
        ->json()['genres'];

        $genres = collect($genreArray)->mapWithKeys(function($genre){

            return [$genre['id'] => $genre['name']];
        });  
    }
}
