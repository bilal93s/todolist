<?php

namespace App\Http\Controllers;

use Faker\ORM\CakePHP\Populator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Auth;
use App\Genre;
use App\Favori;
use App\User;

use Illuminate\Database\Eloquent\Builder;

class FormController extends Controller
{
    public function create()
    {   
        $all = $this->allGenreUnique();
        return view('favoris',['all'=>$all]);
    }

    public function allGenreUnique()
    {
        $showGenres = Genre::getShowsGenres()->toArray();
        $movieGenres = Genre::getMoviesGenres()->toArray();

        $all = array_merge($showGenres, $movieGenres);
        

        $allGenre = array();
        foreach ($all as $genre) {
            if (isset($allGenre[$genre['id']])) {
                continue;
            }
            $allGenre[$genre['id']] = $genre;
        }
        return collect(array_values($allGenre));
    }

    public function add_favoris(Request $request)
    {

        foreach($request->genre as $genre){
            if ($this->check_user_genre($genre)) {    
                $favori = new Favori();
                $favori->id_user = Auth::user()->id;
                $favori->id_genre = $genre;
                $favori->save();
            }
        }
    

        $favoris = Favori::whereNotNull('id_genre')->whereHas('user', function (Builder $query) {
            $query->where('id', '=', Auth::id());
        })->get();



        return redirect('home');

    }

    public function check_user_genre(int $id)
    {
        $favoris = Favori::where('id_genre', '=', $id)->whereHas('user', function (Builder $query) {
            $query->where('id', '=', Auth::id());
        })->get();

        if ($favoris->isNotEmpty()) {
            return false;
        }
        else {
            return true;
        }
    }

}