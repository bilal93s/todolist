<div class="col-md-3">
    <div class="card mb-4 shadow-sm">
    <img class="bd-placeholder-img card-img-top" src="{{$movie->image()}}"> </img>      
        <div class="card-body">
            <p class="card-text">{{\Carbon\Carbon::parse($movie['release_date'])->format('M d, Y')}}</p>
            <p class="card-text">
            @foreach ($movie->genres as $genre)
                {{ $genre->name }}
                @if (!$loop->last) 
                 , 
                @endif
            @endforeach
            </p>
            <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                    <a href="{{ route('showMovie', ['id' => $movie->id]) }}" type="button" class="btn btn-sm btn-outline-secondary">View</a>
                    <div class="wrapper-addFav" style="margin-left: 10px;">
                        <a href="{{ route('addMovieFav', ['id' => $movie->id]) }}" class="btn btn-sm btn-outline-secondary">Ajouter favori</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>