<div class="col-md-3">
    <div class="card mb-4 shadow-sm">
    <img class="bd-placeholder-img card-img-top" src="{{$show->image()}}"> </img>      
        <div class="card-body">
            <p class="card-text">{{\Carbon\Carbon::parse($show['release_date'])->format('M d, Y')}}</p>
            <p class="card-text">
            @foreach ($show->genres as $genre)
                {{ $genre->name }}
                @if (!$loop->last) 
                , 
                @endif
            @endforeach
            </p>
            <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                    <a href="{{ route('showTv', ['id' => $show->id]) }}" type="button" class="btn btn-sm btn-outline-secondary">View</a>
                    <a style="margin-left:10px" href="{{ route('home') }}" type="button" class="btn btn-sm btn-outline-secondary">Retour</a>
                </div>
            </div>
        </div>
    </div>
</div>