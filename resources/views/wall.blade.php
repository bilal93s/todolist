@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Wall</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                    <a href="{{ route('home') }}"> Go to the home.</a>
                    <form action="{{ route('write') }}" method='post'>
                        @csrf                         
                        <label for="fname">First name:</label><br>
                        <input type="text" id="fname" name="fname"><br>
                        <label for="lname">Last name:</label><br>
                        <input type="text" id="lname" name="lname">
                    </form>
                    @foreach ($films as $film)
                        {{ $film->titre }}
                        @if (!empty($film->genre))
                        ({{-- $film->genre->nom --}})
                        @endif
                        <br>
                    @endforeach

                    </from>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection