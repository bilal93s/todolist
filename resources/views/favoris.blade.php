@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Favoris</h1>
        <form class='row' method="POST" action={{ route('add_favoris') }}>
        @csrf
        @foreach ($all as $genre)
            <div class='col-2'>
            <input type='checkbox' value='{{ $genre['id'] }}' name='genre[]'>
            <label for='{{ $genre['name'] }}'>{{ $genre['name'] }}</label>
            </br>
            </div>
        @endforeach
            <div class='col-12'>
            <input type=submit value="enregistrer les favoris">
            </div>
        </form>
    </div>
@endsection