<?php

namespace Tests\Unit;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Str;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    private $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = new User([
            'name' => 'Bilal',
            'email' => 'bilal@swapit.com',
            'password' => 'passpass',
            'birthday' => Carbon::now()->subDecades(21)->toDateString()
        ]);
    }

    public function testUserIsValid()
    {
        $this->assertTrue($this->user->isValid());
    }

    public function testNoValidEmailNoValide()
    {
        $this->user->email = 'Bilal';
        $this->assertFalse($this->user->isValid());
    }

    public function testUserIsTooYoung()
    {
        $this->user->birthday = Carbon::now()->subDecade(1)->toDateString();
        $this->assertFalse($this->user->isValid());
    }
}
