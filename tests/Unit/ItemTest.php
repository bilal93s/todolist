<?php

namespace Tests\Unit;

use App\Models\Item;
use Illuminate\Support\Str;
use PHPUnit\Framework\TestCase;

class ItemTest extends TestCase
{
    private $item;

    protected function setUp(): void
    {
        parent::setUp();

        $this->item = new Item([
            'name' => 'Tache 1',
            'content' => 'Decription de la Tache 1'
        ]);
    }

    public function testIsValid()
    {
        $this->assertTrue($this->item->isValid());
    }

    public function testEmptyName()
    {
        $this->item->name = '';
        $this->assertFalse($this->item->isValid());
    }

    public function testNullName()
    {
        $this->item->name = null;
        $this->assertFalse($this->item->isValid());
    }

    public function testItemContentTooLong()
    {
        $this->item->content = Str::random(2000);
        $this->assertFalse($this->item->isValid());
    }
}
