<?php

namespace Tests\Unit\Services;

use App\Http\Services\EmailService;
use App\Http\Services\TodoListService;
use App\Models\TodoList;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Str;
use Tests\TestCase;

class TodoListServiceTest extends TestCase
{
    use DatabaseMigrations;

    private $sut;
    private $user;
    private $emailServiceMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->emailServiceMock = $this->getMockBuilder(EmailService::class)
            ->onlyMethods(['sendMail'])
            ->getMock();

        $this->sut = new TodoListService($this->emailServiceMock);

        $this->user = factory(User::class)->create();
    }

    public function testCreateTodoList()
    {
        $this->assertTrue($this->sut->createTodoList($this->user, 'Tache1', 'Description tache1'));
    }

    public function testCreateTodoListIsExisting()
    {
        $this->user->todoList()->save(TodoList::make(['name' => 'name', 'description' => 'desc']));
        $this->assertFalse($this->sut->createTodoList($this->user, 'name', 'description'));
    }

    public function testAddItem()
    {
        $this->emailServiceMock->expects($this->once())->method('sendMail');

        $this->user->todoList()->save(TodoList::make(['name' => 'name', 'description' => 'desc']));
        $this->assertTrue($this->sut->addItem($this->user, 'itemName', 'content'));
    }

    public function testAddItemInvalid()
    {
        $this->expectException('Exception');
        $this->expectExceptionMessage('Item est vide ou invalide');

        $this->emailServiceMock->expects($this->never())->method('sendMail');

        $this->user->todoList()->save(TodoList::make(['name' => 'name', 'description' => 'desc']));
        $this->assertFalse($this->sut->addItem($this->user, 'itemName', Str::random(1200)));
    }

    public function testAddNoItemSameName()
    {
        $this->user->todoList()->save(TodoList::make(['name' => 'name', 'description' => 'desc']));
        $this->assertTrue($this->sut->addItem($this->user, 'itemName', 'contentFirstItem'));
        $this->assertDatabaseHas('items', [
            'name' => 'itemName',
            'content' => 'contentFirstItem'
        ]);
        $this->assertFalse($this->sut->addItem($this->user, 'itemName', 'contentSecondItem'));
        $this->assertDatabaseMissing('items', [
            'content' => 'contentSecondItem'
        ]);
    }

    public function testAddItemInvalidUser()
    {
        $this->emailServiceMock->expects($this->never())->method('sendMail');

        $this->user->todoList()->save(TodoList::make(['name' => 'name', 'description' => 'desc']));
        $this->user->email = 'test';
        $this->assertFalse($this->sut->addItem($this->user, 'itemName', 'content'));
    }

}
