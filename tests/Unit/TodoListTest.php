<?php

namespace Tests\Unit;

use App\Models\Item;
use App\Models\TodoList;
use Tests\TestCase;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Str;


class TodoListTest extends TestCase
{
    private $item;
    private $user;
    private $todoList;

    protected function setUp(): void
    {
        parent::setUp();

        $this->item = new Item([
            'name' => 'Tache1',
            'content' => 'Description de la tache1',
            'created_at' => Carbon::now()->subHour()
        ]);

        $this->user = new User([
            'name' => 'Bilal',
            'email' => 'bilal@swapit.com',
            'password' => 'passpass',
            'birthday' => Carbon::now()->subDecades(21)->toDateString()
        ]);

        $this->todoList = $this->getMockBuilder(TodoList::class)
            ->onlyMethods(['itemsCount', 'getLastItem'])
            ->getMock();
        $this->todoList->user = $this->user;
    }

    public function testAddItem()
    {
        $this->todoList->expects($this->once())->method('itemsCount')->willReturn(1);
        $this->todoList->expects($this->any())->method('getLastItem')->willReturn($this->item);

        $canAddItem = $this->todoList->canAddItem($this->item);
        $this->assertNotNull($canAddItem);
        $this->assertEquals('Tache1', $canAddItem->name);
    }

    public function testAddItemFull()
    {
        $this->todoList->expects($this->any())->method('itemsCount')->willReturn(10);

        $this->expectException('Exception');
        $this->expectExceptionMessage('Votre TodoList contient trop ditem');

        $this->todoList->canAddItem($this->item);
    }

    public function testLastItemRecent()
    {
        $this->todoList->expects($this->any())->method('itemsCount')->willReturn(0);

        $recentItem = $this->item->replicate();
        $recentItem->created_at = Carbon::now()->subMinutes(29);
        $this->todoList->expects($this->any())->method('getLastItem')->willReturn($recentItem);

        $this->expectException('Exception');
        $this->expectExceptionMessage('Il faut patientez 30min entre lajout de chaque item');

        $this->todoList->canAddItem($this->item);
    }

    public function testUserNotValid()
    {
        $this->todoList->user->email = 'test';

        $this->todoList->expects($this->any())->method('itemsCount')->willReturn(0);
        $this->todoList->expects($this->any())->method('getLastItem')->willReturn($this->item);

        $this->expectException('Exception');
        $this->expectExceptionMessage('User est vide ou invalide');

        $this->todoList->canAddItem($this->item);
    }

    public function testItemIsNull()
    {
        $this->expectException('Exception');
        $this->expectExceptionMessage('Item est vide ou invalide');

        $badItem = $this->item->replicate();
        $badItem->name = '';

        $this->todoList->canAddItem($badItem);
    }

    public function testIsValid()
    {
        $todoList = TodoList::make([
            'name' => 'Tache1',
            'description' => 'Description de la tache1'
        ]);

        $this->assertTrue($todoList->isValid());
    }

    public function testIsValidNoDescription()
    {
        $todoList = TodoList::make([
            'name' => 'Tache1',
        ]);

        $this->assertTrue($todoList->isValid());
    }

    public function testNoValidNoName()
    {
        $todoList = TodoList::make([
            'description' => 'Description de la tache1'
        ]);

        $this->assertFalse($todoList->isValid());
    }

    public function testNoValidTooLong()
    {
        $todoList = TodoList::make([
            'name' => 'Tache1',
            'description' => Str::random(3000)
        ]);

        $this->assertFalse($todoList->isValid());
    }
}
