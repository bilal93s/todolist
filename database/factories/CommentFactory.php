<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Comment;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
    return [
        'content' => $faker->sentence(3),
        'user_id' => $faker->numberBetween(1,135),
        'commentable_id' => $faker->randomElement([556574,8619,72545,6795,454626,475557,671]),
        'commentable_type' => 'film/serie', 
    ];
});

